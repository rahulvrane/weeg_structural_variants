import sys
import pandas as pd


if len(sys.argv) < 2:
	sys.exit('\n\nUsage: %s <FST OUTPUT from VCFTOOLS on PER SNP basis>\n\n' % sys.argv[0])

fstable = pd.read_csv(sys.argv[1], sep="\t")

#make all FST values absolute - the VCFTOOLS formula allows for directionality in the comparison

fstable.WEIR_AND_COCKERHAM_FST = abs(fstable.WEIR_AND_COCKERHAM_FST)

# calculate how many lines would make the top 5%
lines = int(round(len(fstable) * 0.05))

#extract the top 5% values

top5pc = fstable.sort_values(by="WEIR_AND_COCKERHAM_FST", ascending=False).head(lines)

#export it to a file
top5pc.to_csv("top5pc_"+sys.argv[1],sep="\t",index=False)

print("imported %s and selected top %d SNP's as the top 5percent. Output in file: %s" % (sys.argv[1], lines,"top5pc_"+sys.argv[1]))
