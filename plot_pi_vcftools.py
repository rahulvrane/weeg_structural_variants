#!/bin/python

#  plot_Fst_vcftools.py <PI OUTPUTS from VCFTOOLS: c1,c2,c3> <OUTPUTPREFIX> <chr> <start> <end>
#  
#  Copyright 2014 Rahul Vivek Rane <rahulvrane@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys

if len(sys.argv) < 6:
	sys.exit("""\n\nUsage: %s <Pi OUTPUTS from VCFTOOLS: c1,c2,c3> <OUTPUTPREFIX> <chr> <start> <end>  

#Details about the variables in order:
# 1. We will use Pi outputs generated below (3 population estimations)
# 2. output prefix of our choosing.
# 3. chromosome to plot (3R or 3L)
# 4. Starting coordilate of chromosome to plot 
# 5. End coordinate of chromosome to plot
# We use the START and END variables to visualise only the regions of the genome with unbiased recombination frequency.\n\n""" % sys.argv[0])

#Kennington Top 3 markers associated with In(3R)P

if sys.argv[3] == '3R':
	kenn_x = [12760256, 17122732, 19721178]
	kenn_y = [0.37, 0.37, 0.37]
	in3rp_x = [12401910, 20580518]
	in3rp_y = [0.36,0.36]
	locs = np.array([ 8000000, 12000000, 16000000, 20000000, 24000000, 28000000])
	start = int(sys.argv[4])
	end = int(sys.argv[5])
	
if sys.argv[3] == '3L':
	locs = np.array([4000000,8000000,12000000,16000000,18500000])
	start = int(sys.argv[4])
	end = int(sys.argv[5])




# 200kb non overlapping windows

# listx = list(sys.argv[1].split(","))


# filex=open(listx[0]).readlines()
# bin_1x = [ (int(filex[x].split()[2]) - int(filex[x].split()[1]) -1)/2 + int(filex[x].split()[1]) for x in range(1, len(filex))]
# fst_1x = [float(filex[x].split()[4].strip("-")) for x in range(1, len(filex))]

# filex=open(listx[1]).readlines()
# bin_2x = [ (int(filex[x].split()[2]) - int(filex[x].split()[1]) -1)/2 + int(filex[x].split()[1]) for x in range(1, len(filex))]
# fst_2x = [float(filex[x].split()[4].strip("-")) for x in range(1, len(filex))]

# filex=open(listx[2]).readlines()
# bin_3x = [ (int(filex[x].split()[2]) - int(filex[x].split()[1]) -1)/2 + int(filex[x].split()[1]) for x in range(1, len(filex))]
# fst_3x = [float(filex[x].split()[4].strip("-")) for x in range(1, len(filex))]



# 200kb windows sliding every 50kb
ylim1 = []
listy = list(sys.argv[1].split(","))
filex=open(listy[0]).readlines()
bin_1 = [ (int(filex[x].split()[2]) - int(filex[x].split()[1]) -1)/2 + int(filex[x].split()[1]) for x in range(1, len(filex))]
fst_1 = [float(filex[x].split()[4].strip("-")) for x in range(1, len(filex))]
ylim1.append(max(fst_1))

filex=open(listy[1]).readlines()
bin_2 = [ (int(filex[x].split()[2]) - int(filex[x].split()[1]) -1)/2 + int(filex[x].split()[1]) for x in range(1, len(filex))]
fst_2 = [float(filex[x].split()[4].strip("-")) for x in range(1, len(filex))]
ylim1.append(max(fst_2))


filex=open(listy[2]).readlines()
bin_3 = [ (int(filex[x].split()[2]) - int(filex[x].split()[1]) -1)/2 + int(filex[x].split()[1]) for x in range(1, len(filex))]
fst_3 = [float(filex[x].split()[4].strip("-")) for x in range(1, len(filex))]
ylim1.append(max(fst_3))
#Prep Plot
total_max = max(ylim1)

plt.figure(figsize=(10, 3.5),facecolor="white")
# #Plot 1
# plt.subplot(2,1,1)
# plt.plot(bin_1x, fst_1x, color='#e41a1c', label= str(listx[0]).split("_")[0], linewidth=1.5)
# plt.plot(bin_2x, fst_2x, color='#377eb8', label=str(listx[1]).split("_")[0], linewidth=1.5)
# plt.plot(bin_3x, fst_3x, color='#4daf4a', label=str(listx[2]).split("_")[0], linewidth=1.5)

# plt.xlim(start, end)
# plt.legend(loc='upper right', fontsize='medium')

# plt.xlabel('chromosome location on ' + sys.argv[4] + ' in MB')
# plt.ylabel('Weighted Fst')
# plt.title('Weighted Fst - 200kb windows - nonoverlapping', fontsize='medium')
# plt.xticks(locs, map(lambda x: "%.1f" % x, locs/1e6))
# if sys.argv[4] == '3R':
	# plt.ylim(ymax = 0.3)
	# plt.plot(kenn_x, kenn_y, 'or')
	# plt.plot(in3rp_x, in3rp_y, color='black', linewidth=2)
	# plt.axvspan(in3rp_x[0], in3rp_x[1], color='gray', alpha=0.2)
#redo the limits for markers


if sys.argv[3] == '3R':
	kenn_x = [12760256, 17122732, 19721178]
	kenn_y = [total_max+0.0001,total_max+0.0001,total_max+0.0001]
	in3rp_x = [12401910, 20580518]
	in3rp_y = [total_max+0.0001,total_max+0.0001]

# Plot 2
plt.subplot(1,1,1)
plt.plot(bin_1, fst_1, color='#e41a1c', label=str(listy[0]).split("_")[0], linewidth=1.5)
plt.plot(bin_2, fst_2, color='#377eb8', label=str(listy[1]).split("_")[0], linewidth=1.5)
plt.plot(bin_3, fst_3, color='#4daf4a', label=str(listy[2]).split("_")[0], linewidth=1.5)

plt.xlim(start, end)
plt.legend(loc='upper right', fontsize='medium')
#TODO: add pi and D variables
plt.xlabel('chromosome location on ' + sys.argv[3] + ' in MB')
plt.ylabel('Mean pi')
plt.title('Mean pi - 200kb windows', fontsize='medium')
plt.xticks(locs, map(lambda x: "%.1f" % x, locs/1e6))
if sys.argv[3] == '3R':
	plt.ylim(ymax = total_max + 0.0001)
	plt.plot(kenn_x, kenn_y, 'or')
	plt.plot(in3rp_x, in3rp_y, color='black', linewidth=2)
	plt.axvspan(in3rp_x[0], in3rp_x[1], color='gray', alpha=0.2)

plt.tight_layout()
plt.savefig(sys.argv[2] + '.png')
plt.savefig(sys.argv[2] + '.svg')
plt.savefig(sys.argv[2] + '.pdf')

